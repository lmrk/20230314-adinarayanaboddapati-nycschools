//
//  HighSchoolDetailDataViewModel.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import Foundation

class HighSchoolDetailDataViewModel {
    
    
    var updateUI: ((_ highSchools: [HighSchoolDetailModel], _ error: Error?) -> Void)?
    
    var schoolDetailData = [HighSchoolDetailModel]()
    
    func fetchSchoolData(params: [String: String]) {
        NetworkManager.shared.request(fromURL: AppURLs.highScoolDetailAPI, parms: params) { [weak self] (result: Result<[HighSchoolDetailModel], Error>) in
            switch result {
            case .success(let school):
                self?.schoolDetailData = school
                self?.updateUI?(school, nil)
            case .failure(let error):
                self?.updateUI?([], error)
            }
        }
    }
}
