//
//  SchoolDetailViewController.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    @IBOutlet weak var schoolNameLbl: UILabel!
    @IBOutlet weak var mathScoreLbl: UILabel!
    @IBOutlet weak var readingScoreLbl: UILabel!
    @IBOutlet weak var writingScoreLbl: UILabel!
    @IBOutlet weak var noOfTestTakersLbl: UILabel!


    var dbn: String = ""
    let schoolDetailVM = HighSchoolDetailDataViewModel()
    
    lazy var spinner: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = UIColor.red
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "High Scholl SAT Details"
        // Do any additional setup after loading the view.
        schoolDetailVM.updateUI = { [weak self] ( detailModel, error) in
            DispatchQueue.main.async {
                self?.spinner.stopAnimating()
                guard error == nil else {
                    self?.showAlert("Something went wrong, please try again.", completion: nil)
                    return
                }
                guard !detailModel.isEmpty else {
                    self?.showAlert("No data to display", completion: { action in
                        self?.navigationController?.popViewController(animated: true)
                    })
                    return
                }
                self?.displayData()
            }
        }
        
        spinner.startAnimating()
        schoolDetailVM.fetchSchoolData(params: ["dbn": dbn])
    }
    
    private func displayData() {
        self.schoolNameLbl.text = schoolDetailVM.schoolDetailData.first?.schoolName
        self.mathScoreLbl.text = schoolDetailVM.schoolDetailData.first?.mathematicsMean
        self.readingScoreLbl.text = schoolDetailVM.schoolDetailData.first?.criticalReadingMean
        self.writingScoreLbl.text = schoolDetailVM.schoolDetailData.first?.writingMean
        self.noOfTestTakersLbl.text = schoolDetailVM.schoolDetailData.first?.numberOfTestTakers
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
