//
//  HighSchoolsViewController.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import UIKit

class HighSchoolsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    private let reuseIdentifier = "cell"
    private let schoolDetailSegueIdentifier = "schollDetail"

    let viewModel = HighSchoolDataViewModel()

    lazy var spinner: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = UIColor.red
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        viewModel.updateUI = { [weak self] (_, error) in
            DispatchQueue.main.async {
                self?.spinner.stopAnimating()
                self?.tblView.reloadData()
            }
        }
        spinner.startAnimating()
        viewModel.fetchData()
    }

    private func displayData() {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? SchoolDetailViewController
        vc?.dbn = sender as? String ?? ""
    }

}

extension HighSchoolsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getNumberOfItems()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath)
        cell.selectionStyle = .none
        var content = cell.defaultContentConfiguration()
        content.text = viewModel.getTitleFor(indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.contentConfiguration = content
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: schoolDetailSegueIdentifier, sender: viewModel.getDBNFor(indexPath))
    }
}
