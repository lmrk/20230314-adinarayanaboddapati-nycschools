//
//  HighSchoolDataModel.swift
//  20230314-AdinarayanaBoddapati-NYCSchools
//
//  Created by Aadi on 3/14/23.
//

import Foundation

struct HighSchoolDataModel: Codable {
    let dbn: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
    }
}
