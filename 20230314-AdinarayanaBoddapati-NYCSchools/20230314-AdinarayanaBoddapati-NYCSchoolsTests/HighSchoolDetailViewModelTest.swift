//
//  HighSchoolDetailViewModelTest.swift
//  20230314-AdinarayanaBoddapati-NYCSchoolsTests
//
//  Created by Aadi on 3/15/23.
//

import XCTest
@testable import _0230314_AdinarayanaBoddapati_NYCSchools

final class HighSchoolDetailViewModelTest: XCTestCase {
    var sut: HighSchoolDetailDataViewModel!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = HighSchoolDetailDataViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }

}
