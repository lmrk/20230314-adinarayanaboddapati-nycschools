//
//  _0230314_AdinarayanaBoddapati_NYCSchoolsTests.swift
//  20230314-AdinarayanaBoddapati-NYCSchoolsTests
//
//  Created by Aadi on 3/14/23.
//

import XCTest
@testable import _0230314_AdinarayanaBoddapati_NYCSchools

final class ApiCallsTest: XCTestCase {
    
    var sut: NetworkManager!
    override func setUpWithError() throws {
        sut = NetworkManager.shared
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testHighSchoolAPI() {
        let exp = self.expectation(description: "com.highschool.api")
        sut.request(fromURL: AppURLs.highScoolsAPI, parms: nil) {  (result: Result<[HighSchoolDataModel], Error>) in
            switch result {
            case .success(let schools):
                XCTAssert(schools.count > 0, "No data to display")
                exp.fulfill()
            case .failure(let error):
                XCTFail("Expected Data but failed with error: \(error.localizedDescription)")
                exp.fulfill()
            }

        }
        self.wait(for: [exp], timeout: 5)
    }
    
    func testHighSchoolDetailAPI() {
        let exp = expectation(description: "com.highschooldetail.api")
        sut.request(fromURL: AppURLs.highScoolDetailAPI, parms: nil) { (result: Result<[HighSchoolDetailModel], Error>) in
            switch result {
            case .success(let schools):
                XCTAssert(schools.count > 0, "No data to display")
                exp.fulfill()
            case .failure(let error):
                XCTFail("Expected Data but failed with error: \(error.localizedDescription)")
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: 5)
    }
}
